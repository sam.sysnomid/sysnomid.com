---
layout: page
title: Projects
---

## About

Hello, I'm Sysnomid, a hobbyist developer.

<a href="/pages/about">Contact Details</a>

GitHub: [https://github.com/Sysnomid](https://github.com/Sysnomid)

GitLab: [https://gitlab.com/sam.sysnomid](https://gitlab.com/sam.sysnomid)

## Stuff I've Made

- ### [paste.sysnomid.com - Personal Pastebin](https://paste.sysnomid.com)

[GitHub](https://github.com/Sysnomid/paste)

<br />

- ### [6nmd.us - Vue Url Shortener](https://6nmd.us)

[GitHub](https://github.com/Sysnomid/6nmd-Vue-Urls)

<br />

- ### [crypto.sysnomid.com - Vue Crypto Dash](https://crypto.sysnomid.com)

[GitHub](https://github.com/Sysnomid/Vue-Crypto-Dash)

<br />

- ### [next-ts-tailwind NPM package](https://www.npmjs.com/package/next-ts-tailwind)

[GitHub](https://github.com/Sysnomid/next-ts-tailwind)

<br />

- ### [The Devs' Guild Website](https://thedevsguild.com)

[GitHub](https://github.com/Sysnomid/GuildSite)

<br />

- ### [Up to 11ty Theme](https://upto11ty.vercel.app/)

[GitHub](https://github.com/Sysnomid/UpTo11ty)

## Stuff I Use

**Languages**: Javascript, Typescript, Golang, SQL, Python

**Frameworks**: ExpressJS, VueJS, NextJS, ReactJS, 11ty, Golang Echo

**Deployment Tools**: Vercel, Netlify, Nginx, Docker, Dokku, AWS

**Other Tools**: Ubuntu and Debian Linux, Git and GitHub
