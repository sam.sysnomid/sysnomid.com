## Sysnomid.com

My personal blog, https://sysnomid.com, built with 11ty.

## Usage:

    git clone https://github.com/Sysnomid/sysnomid.com.git

    npm install

    npm run dev or npm start

Blog content under CC-BY-4.0 license, https://creativecommons.org/licenses/by/4.0/
